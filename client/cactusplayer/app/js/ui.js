
/* Default config settings. */
if(typeof(localStorage.CONFIG_reward_track_loaded) === 'undefined'){
	localStorage.CONFIG_reward_track_loaded = true;
}
if(typeof(localStorage.CONFIG_proxy_url) === 'undefined'){
	localStorage.CONFIG_proxy_url = '';
}
if(typeof(localStorage.CONFIG_enable_lastfm_scrobbling) === 'undefined'){
	localStorage.CONFIG_enable_lastfm_scrobbling = false;
}
if(typeof(localStorage.CONFIG_enable_lastfm_scrobbling) === 'undefined'){
	localStorage.CONFIG_enable_lastfm_scrobbling = false;
}

$(document).ready(function(){

	

	//Keep track of mouse position at all times.
	global.currentMousePos = {};
	$(document).mousemove(function(event) {
        global.currentMousePos.x = event.pageX;
        global.currentMousePos.y = event.pageY;
    });

   

	$('#searchform').submit(function(){
		var searchquery = $('#searchfield').val();
		console.log('searching for:'+searchquery)

		searchTrack(searchquery);
		event.preventDefault();
	});

	$('#playnext').click(function(){
		playNextTrack();
	});
	$('#playprevious').click(function(){
		playPrevTrack();
	});

	$('#playpause').click(function(){
		togglePlayPause();
	});
	$("#shuffle").click(function(){

		toggleShuffle();
		if(global.shuffle_mode === true){
			$("#shuffle").addClass('on');
		}else{
			$("#shuffle").removeClass('on');
		}
	});

	$(window).keyup(function(){
		if(event.which == 32 && $('input:focus').length < 1){//Space
			togglePlayPause();
			event.preventDefault();
		}
	});
	

	function togglePlayPause(){
		if(player.playing !== undefined){
			if(!player.playing){
				player.play();
				$('#playpause').html('<i class="glyphicon glyphicon-pause"></i>');
			}else{
				player.pause();
				$('#playpause').html('<i class="glyphicon glyphicon-play"></i>');
			}
		}else{
			$('.playlistwrapper .playlistsong:first').trigger("staticClick");
		}
	}

	

	$(".progressbar").click(function(e){

		//If duration is known.
		if(typeof player.duration !== 'undefined' && player.duration > 0){
			//Get click position in bar
			//var offset = $(this).offset(); 
		    //or $(this).offset(); if you really just want the current element's offset
		    //var offset = $(this).offset();
		    //var relX = e.pageX - offset.left;
		    //var relY = e.pageY - offset.top;
		    //console.log(relX);
		    //var percentage = relX / $(this).width();
		    var percentage = getPercentageMouseOffset(e,$(this));
		    if(player.buffered < percentage){//Dont move when clicking after the part that was buffered, as this will result in an error.
		    	return false;
		    }
		    console.log('percentage:'+percentage);
		    console.log('seek:'+~~(player.duration*percentage));
		    player.pause();
		    setTimeout(function(){
		    	try{
					player.seek(~~(player.duration*percentage));
					player.play();
				}catch(e){//Sometimes crashes, depending on file type/contents.
					showNotice("<strong>Player Error:</strong> Seeking did not go well. Probably not supported for this file type.","danger");
					//player.stop();
					stopPlayerGracefully()
				}
		    }, 100);
			
		}

	});

	$('.volumecontrol_bar').click(function(e){
		var volume = getPercentageMouseOffset(e,$(this));
		setVolume(100*volume);
	});
	setVolume(50);


	$('#save_playlist_button').click(function(){
		var tracks = [];
		$.each(global.playlist, function(i,track){
			tracks[i] = serializeTrack(track) ;
		});

		saveFile('#export_playlist', JSON.stringify({current_index: global.PLAYLIST_INDEX, tracks: tracks}));
	});


  $('#load_playlist_button').click(function(){
	  openFile('#import_playlist', function(file_contents){
	  	var playlist = JSON.parse(file_contents);
	  	console.log(playlist);
	  	//First make current playlist empty.
	  	clearPlaylist();
	  	/*setTimeout(function(){
	  		for(var i=0;i<playlist.tracks.length;i++){
	  			setTimeout(function(){
		  			appendTrackToPlaylist(playlist.tracks[i]);
		  			grabAlbumCoverImageUrl($('.albumimage', $('.playlistsongs:eq('+i+')')),playlist.tracks[i]);//For some reason, doesn't work when added before.
		  		},100);
		  	};
		  	global.PLAYLIST_INDEX = playlist.current_index -1;
		  	//$('.playlistsongs:eq('+global.PLAYLIST_INDEX+')').addClass('currentlyplaying');
		  	playNextTrack();

	  	},500);*/

	  appendAlbumToPlaylist(playlist.tracks);
	  global.PLAYLIST_INDEX = playlist.current_index;
	  $('.playlistwrapper .playlistsong:eq('+global.PLAYLIST_INDEX+')').trigger('staticClick');
	  	

	  });
	});
	$('#flush_playlist_button').click(function(){
		clearPlaylist();
	});



	











	Tipped.create('.playbuttons button, .volumecontrol_bar, small_button', {position: "top", behavior: "hide"});
	Tipped.create('.bar_button', {position: "top", offset: {y: -10}});
	Tipped.create('#searchfield', {position: "bottom", offset: {x:20}});
	Tipped.create('.progressbuffered',	"<span class='seek_info'>Seek:<strong>?</strong></span>",	 {behavior: "mouse", position:"top"});
	Tipped.create('#select_host', $('.known_hosts_list')[0], {title:"Choose a server to connect to:"});

	Tipped.create('#add_torrent',"Add New Album to the Network");
	$("#add_torrent").click(function(){
		//gui.Shell.openExternal($.trim(global.server_location + 'parser/new_torrent_form.php'))
		addTorWin = new gui.Window.open('add_torrent.html', {width:1200,height:900,toolbar:false,focus:true, resizable:true});


	});

	Tipped.create('#settings_open_btn',"Settings");
	Tipped.create('#settings_open_btn',"Settings");
	Tipped.create('.settings_label');
	$("#settings_open_btn").click(function(){$('#settings_modal').modal('show')});
	$("#save_settings_btn").click(saveSettings);
	$("#reset_settings_modal").click(resetSettings);//Reset settings
	$('#settings_modal').on('hide.bs.modal',resetSettings);//Reset settings as well.

	$("#remove_favourites_btn").click(function(){
		if(!confirm($(this).data("confirm"))){
			return;
		}
		favdb.removeAll(function(){
			$('.songcontainer, .playlistsong').removeClass('favourited');
			showNotice("All favourites have been cleared.","info");
		});
	});

	$("#authorize_lastfm").click(requestLastFmAuthorize);
	$("#check_lastfm_connection").click(checkLastFmAuthorisation);





	Tipped.create('#show_most_popular',"Most Popular Songs (on this server)", {hideOthers:true});
	$('#show_most_popular').click(showMostPopularSongs);
	Tipped.create('#show_newest_songs',"Recently Added Songs (to this server)", {hideOthers:true});
	$('#show_newest_songs').click(showNewestSongs);

	
	Tipped.create('#show_favourites',"Your Favourites", {hideOthers:true});
	$('#show_favourites').click(showFavouritesList);

	$('.progressbuffered').mousemove(function(e){
		var percentage = getPercentageMouseOffset(e,'.progressbar');
		
		var result =  "<span class='seek_info'>Seek:<strong>"+mSecToTime(player.duration*percentage)+"</strong></span>";
		$('.tpd-tooltip .seek_info').html(result);
	});


	loadSettings();




	$("h1.title").click(loadHomeView);
	
	loadHomeView();


});

function loadHomeView(){
	$('.searchresults_title').html('');
	$('#searchresults').load('home_view.html');
}

function serializeTrack(track){
	return {
		urn: track['urn'], 
		file_indexnumber: track['file_indexnumber'],
		albumname: track['albumname'], 
		artist: track['artist'], 
		genres: track['genres'], 
		trackname: track['trackname'],
		filename: track['filename'] 
	}
}

function clearPlaylist(){

	/*$('.currentlyplaying').removeClass('currentlyplaying');
	for(var i=0;i<global.playlist.length;i++){
		setTimeout(function(){
			removeTrackFromPlaylist(0);
			global.playlist.shift();
		},100);
	};*/
	/*try{
		player.stop();
	}catch(e){};*/
	stopPlayerGracefully()


	$('.playlistcontainer .playlistsong').fadeOut(400, function(){
		$(this).remove();
	});
	setTimeout(function(){
		$('.playlistcontainer').packery('reloadItems');
	    loadReorderedPlaylist();
	},500);

	/*setTimeout(function(){

		$('.playlistcontainer').packery();
	    loadReorderedPlaylist();
	},100);*/
}

function setVolume(volume){
	global.PLAYER_VOLUME = volume;
	if(typeof player !== 'undefined'){
		player.volume = volume;
	}
	$('.volumecontrol_volume','.volumecontrol_bar').css('width', volume+'%');
	console.log('Volume:', volume);
}

function getPercentageMouseOffset(e,elem){
	var offset = $(elem).offset();
	if(typeof(e)==='undefined'){
		var relX = global.currentMousePos.x - offset.left;
	}else{
	    var relX = e.pageX - offset.left;
	}
    //var relY = e.pageY - offset.top;
    //console.log(relX);
    return relX / $(elem).width();
}

function saveFile(name,data) {
    var chooser = document.querySelector(name);
    chooser.addEventListener("change", function(evt) {
        console.log(this.value); // get your file name
      	var fs = require('fs');// save it now
		fs.writeFile(this.value, data, function(err) {
		    if(err) {
		       alert("error"+err);
		    }
		});
    }, false);
    chooser.click();  
}

function openFile(name, callback) {
    var chooser = document.querySelector(name);
    chooser.addEventListener("change", function(evt) {
      console.log(this.value);
      var fs = require('fs');// load it now
		fs.readFile(this.value, {encoding: 'utf8'}, function(err, data) {
		    if(err) {
		       alert("error"+err);
		    }else{
		    	console.log("File Contents:",data);
		    	callback(data);
		    }
		});
    });

    chooser.click();
}

function resetSettings(){
	if(	   (localStorage.CONFIG_proxy_url != $('input#proxy_url').val())
		|| (localStorage.CONFIG_reward_track_loaded != $('input#reward_track_loaded').prop( "checked" ).toString())
	){
		showNotice("Settings discarded.", "warning");
	}
	loadSettings();
}

function loadSettings(){
	
	$('input#reward_track_loaded').prop('checked',localStorage.CONFIG_reward_track_loaded=="true");
	$("input#enable_lastfm_scrobbling").prop("checked", localStorage.CONFIG_enable_lastfm_scrobbling=="true");

	setTimeout(function(){//GUI gets defined on document ready, so call this áfter.
		$('input#proxy_url').val(localStorage.CONFIG_proxy_url);
		gui.App.setProxyConfig(localStorage.CONFIG_proxy_url);
		HTTP_PROXY = localStorage.CONFIG_proxy_url; //Set the environment var that 'require' uses.
		if(HTTP_PROXY==''){
			HTTP_PROXY=undefined;
		}
	},1);


	if(localStorage.CONFIG_enable_lastfm_scrobbling=="true"){
		global.lastfm_session = lastfm.session({token:localStorage.last_fm_auth_token,
			handlers:{
				error:function(error){
					showNotice("<strong>Last.fm Failure!</strong> Connecting failed: "+error.message, "danger");
					global.lastfm_session = undefined;	
				}

		}});
	}
}

function saveSettings(){
	showNotice("<strong>Settings</strong> saved successfully.", "success");
	localStorage.CONFIG_reward_track_loaded = $('input#reward_track_loaded').prop( "checked" );
	localStorage.CONFIG_enable_lastfm_scrobbling = $("input#enable_lastfm_scrobbling").prop("checked");

	saveProxySettings();
}

function saveProxySettings(){
	if(localStorage.CONFIG_proxy_url != $('input#proxy_url').val()){
		localStorage.CONFIG_proxy_url = $('input#proxy_url').val();
		gui.App.setProxyConfig(localStorage.CONFIG_proxy_url);
		HTTP_PROXY = localStorage.CONFIG_proxy_url; //Set the environment var that 'require' uses.
		if(HTTP_PROXY==''){
			HTTP_PROXY=undefined;
		}
	}
}


//Called whenever a user clicks a button in the 'settings' menu. Links to last.fm site where user needs to grant our app access.
function requestLastFmAuthorize(){
	var req = lastfm.request('auth.gettoken'); 
	req.on("error",function(error){
		showNotice("<strong>Last.fm Error:</strong> Authorization failed: "+error.message,'danger');
	})
	req.on("success", function(data){
		console.log(data['token']);
		
		//Store in localStorage until next request.
		localStorage.last_fm_auth_token = data['token'];
		
		//Now open up external address.
		gui.Shell.openExternal("http://www.last.fm/api/auth/?api_key="+last_fm_api_key+"&token="+data['token']);
	});
}
function checkLastFmAuthorisation(){
	if(localStorage.CONFIG_enable_lastfm_scrobbling!="true"){
		showNotice("Scrobbling is disabled.","danger");
		return;
	}

	lastfm.session({token:localStorage.last_fm_auth_token,
	handlers:{
		success:function(session){
			showNotice("Congrats! Cactus Player is authorized to use Last.fm", "success");
		},
		error:function(error){
			showNotice("Failure! Cactus Player is not authorized to use Last.fm:"+error.message, "danger", 0);
		}

	}});
}






$(document).ready(function(){
	$('.back_to_results_link').hide();
	$('.back_to_results_link').click(function(e){
		try{
		    e.preventDefault();
			console.log("Back Link was clicked!");
		    $("#searchresults").html('');
		    $("#searchresults").attr("data-prevent-loadmore",false);
			$('.searchresults_title').html(global.searchresults_title);
		    showSearchResultTracks(global.all_searchresults);
	    }catch(e){
	    	
	    }
	    

	    $('.back_to_results_link').fadeOut();
	});
});
