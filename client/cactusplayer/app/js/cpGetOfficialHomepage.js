function gotoOfficialArtistHomepage(artistname){
	getOfficialArtistHomepage(artistname, function(url){
		gui.Shell.openExternal(url);
	},function(){
		showNotice("The Artist's Homepage could not be found.", 'info');

	});
}
function getOfficialArtistHomepage(artistname, callback ,callFail){
	var artistname = artistname.latinize();//Remove accents like Ó because they make it often impossible to find a result.
	if(callback === undefined){
		callback = function(){};
	}
	if(callFail === undefined){
		callFail = function(){};
	}

	var mb_root_url = "http://musicbrainz.org/ws/2/";


	request(mb_root_url+'artist/?query=artist:'+artistname+'&fmt=json',function(err,response,body){
		try{
			//console.log(body);
			var json = $.parseJSON(body);
			//console.log(json.artists);
			//console.log(json.artists[0]);
			//console.log(json.artists[0].id);
			var mbid = json.artists[0].id;
			}catch(e){
				console.log(e);
				//return false;
				callFail();
			}
			request(mb_root_url+'artist/'+mbid+'?inc=url-rels&fmt=json',function(err,response,body){
				//console.log('result!');
				//console.log(body);
				try{
					var artist = $.parseJSON(body);
					found = false;
					var url = '';
					artist.relations.forEach(function(rel,index){
						console.log(rel.type);
						if(rel.type=='official homepage'){
							url = rel.url.resource;
							found = true;
						}
					});
					if(!found){
						callFail();
						//return false;
					}else{
						console.log(url);
						callback(url);
					}
				}catch(e){
					console.log(e);
				}
			});
		
	});


	
}
function gotoLastFmPage(albumname, artist){
var request = lastfm.request("album.search", {
                    album: albumname +" "+ artist,
                    handlers: {
                        success: function(data) {
                            console.log("Success: ", data);
                            console.log(data.results.albummatches);
                            try{
                                var address = data.results.albummatches.album[0].url;
                                gui.Shell.openExternal(address);
                            }catch(e){
                                showNotice("No Last.fm page found for this album.","info");
                            }
                        },
                        error: function(error) {
                            showNotice("Error while getting Last.fm page: "+error.message,"danger");
                        }
                    }
                });

}

$(document).ready(function(){
	$('.musicBrainzLink').click(function openMusicBrainzLink(e){
	    gui.Shell.openExternal('http://musicbrainz.org/');e.preventDefault();
	});
});