<?php
	session_start();
	
	require_once '../config.php' ;
	require_once '../database.php';

	require_once 'parser_functions.php';


	$ROOT_TO_CURRENT = '../';
	include "../header.php";

?>
<div class="content_wrapper">
	<?php


	if(isset($_POST['URN']) ){
		preg_match("/xt=urn:btih:([A-Ha-h0-9]+)&/", $_POST['URN'], $magnet_matches);
		if(isset($magnet_matches[1])){
			$urn = $magnet_matches[1];
		}else{
			$urn = $_POST['URN'];
		}
		//print_r($magnet_matches);
		//echo "Searching Torrent ".$_POST['URN'] ."...";
		$torrentinfo = getTorrentFile(trim(strtoupper($urn)));
		$clean_album_name = cleanFilename($torrentinfo['info']['name']);
		$musicalfiles = array();

		if(count($torrentinfo['info']['files']) < 1){
			echo "<p class='error'>Error: Either the torrent file is invalid (could not be found), or it does not contain separate files, which CactusPlayer needs to play the separate tracks.</p>";
			unset($_SESSION['torrent_data']);
			
		}else{

		foreach($torrentinfo['info']['files'] as $index=>$file){

			$extension = pathinfo($file['path'][count($file['path'])-1], PATHINFO_EXTENSION );
			if($extension == 'mp3' || $extension == 'wav' || $extension == 'ogg' || $extension == 'flac' || $extension == 'aac'){
				$filename = pathinfo($file['path'][count($file['path'])-1], PATHINFO_BASENAME );

				$filename = cleanFilename($filename, false);
				
				//print_r($file);

				$is_musical = true;
				$fullfile = $file;
				$fullfile['index'] = $index;
				$fullfile['filename'] = $filename;
				array_push($musicalfiles,$fullfile);

			}
		}

		$discogs_album = getDiscogsAlbumSearch($clean_album_name);

		$_SESSION['torrent_data'] = array();
		$_SESSION['torrent_data']['URN'] = $urn;
		$_SESSION['torrent_data']['musicalfiles'] = $musicalfiles;
		$_SESSION['torrent_data']['discogslink'] = $discogs_album['resource_url'];

		$torrentname 	= $_SESSION['torrent_data']['torrentname'] = $torrentinfo['info']['name'];
		$artist_name 	= $discogs_album['artists'][0]['name'];
		$genres 		= is_array($discogs_album['genres'])?implode($discogs_album['genres'], ', ') : '';


//		var_dump($musicalfiles);

?>
<div>

<h2>Edit Album Information</h2>

<form name="step2" method="POST" action="new_torrent_form.php" class="new_torrent_edit_form">
	<div class="important_forminfo">
	<h4>URN: <?php echo $urn; ?> </h4>
	<h4>Torrent Name: <?php echo $torrentname; ?> </h4>
	
		<div class="island center">
			<label for="albumname">
				Album Name:
			</label>
			<input name="albumname" id="albumname" value="<?php echo $clean_album_name; ?>"/>

		</div>
		<div class="island center">
			<label for="artistname">
				Artist:
			</label>
			<input name="artistname" id="artistname" placeholder=""/>
			<button id="removeArtistName" type="button">Remove Artist Name From Song Names</button>

		</div>

		<div class="island center">
			<label for="genres">
				Genre(s):
			</label>
			<input name="genres" id="genres" value="<?php echo $genres ?>"/>
			<em>(Separate with comma's)</em>
		</div>
	</div>


<?php 	foreach($musicalfiles as $file_index=>$filedata){?>
	
			<div class="island">
				<input type="text" name="<?php echo "trackname_".$file_index; ?>" class="trackname" value="<?php echo $filedata['filename']; ?>"/>
				<em><?php echo implode($filedata['path'], '/') ?></em>
			</div>
		
<?php 	} ?>

<input type="submit" value="Save Album" />
</form>


<script>
function removeArtistName(){
	var inputs = $('input.trackname');
	var artistname = $('input#artistname').val();
	var regexp = new RegExp('( - )?'+artistname+'( - )?','gi');
	console.log(regexp);
	inputs.each(function(i, elem){
		var value = $(elem).val();
		console.log('value:',value);
		$(elem).val(value.replace(regexp, ''));
	});
}
$(document).ready(function(){
	$('button#removeArtistName').click(removeArtistName);
});
</script>

</div>
<?php

}
	}else{
		if(isset($_SESSION['torrent_data']) && isset($_POST['albumname']) && isset($_POST['artistname'])){

			//echo "Storing Album into database...<br/>";

			//print_r($_SESSION['torrent_data']);
			//foreach($_SESSION['torrent_data']['musicalfiles'] as $index=>$file){
				//print_r($_POST['trackname_'.$index]);
			//}

			if(count($_SESSION['torrent_data']['musicalfiles']) < 1){
				echo "<p class='error'>Error: Either the torrent file is invalid (could not be found), or it does not contain separate files, which CactusPlayer needs to play the separate tracks.</p>";
				unset($_SESSION['torrent_data']);
				die();
			}
			//print_r($_SESSION['torrent_data']);

			$urn = $_SESSION['torrent_data']['URN'];
			$torrentname = $_SESSION['torrent_data']['torrentname'];
			$albumname = trim($_POST['albumname']);
			$artistname = trim($_POST['artistname']);
			$genres = trim($_POST['genres']);

			insertNewPeerMusicAlbum($urn, $torrentname, $albumname, $artistname, $genres, $_SESSION['torrent_data']['discogslink']);
			$album_id = mysql_insert_id();
			//echo "<p class='error'>Note: Album ID is: <strong>$album_id</strong></p>";

			foreach($_SESSION['torrent_data']['musicalfiles'] as $index=>$file){
				$filename = pathinfo($file['path'][count($file['path'])-1], PATHINFO_BASENAME );
				$trackname = trim($_POST['trackname_'.$index]);
				$file_indexnumber = $file['index'];

				insertNewPeerMusicTrack($filename, $trackname, $file_indexnumber, $album_id);
				//echo "<p class='success'>Song: <strong>". $trackname. "</strong> was successfully entered into the database.</p>";
			}
		unset($_SESSION['torrent_data']);

		echo "<p class='success'>The album <strong>" . $albumname . "</strong> was saved successfully!</p>";

		echo "</p>";
?>

<?php } ?>
<div>
	<h2>Fill In the Torrent's Magnet Link or URN to start:</h2>
	<form class="new_torrent_form" name="step1" method="POST" action="new_torrent_form.php">
		<input name="URN" id="URN" placeholder="magnet:?xt=urn:btih:9e33237b77f0696645bb4f8640eb5ebccd742a2e"/>
		<input type="submit" value="Make Album" />
		<p>Need Help? <a href="../faq.php#add_album_guide" target="_blank">Check the guide!</a></p>
	</form>
</div>
<?php } ?>
</div>

<?php
include "../footer.php";
?>