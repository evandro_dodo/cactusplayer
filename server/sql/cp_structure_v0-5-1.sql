-- MySQL dump 10.13  Distrib 5.5.32, for Win32 (x86)
--
-- Host: localhost    Database: cactusplayer
-- ------------------------------------------------------
-- Server version	5.5.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `p2pm_albums`
--

DROP TABLE IF EXISTS `p2pm_albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p2pm_albums` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `urn` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Torrent''s Unique Resource Name. Can uniquely identify a torrent on the network, and also neccesary to make a magnet link to that location.',
  `torrentname` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Visible name of the torrent. Set in the torrent client where the torrent is made.',
  `albumname` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Name of the album.',
  `genres` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Comma-separated genres that will also be used when searching for this album/track.',
  `artist` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Name of the artist.',
  `discogs_id` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Deprecated; Will probably be removed.',
  `inserted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `album_plays` int(11) NOT NULL DEFAULT '0' COMMENT 'Increment at each successful client play. Higher means more popular and better torrent health.',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `p2pm_tracks`
--

DROP TABLE IF EXISTS `p2pm_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p2pm_tracks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `filename` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Full file path in torrent to file. Includes file extension as well.',
  `file_indexnumber` int(11) NOT NULL COMMENT 'The index number of the file in the torrent. Used for Uniquely Identifying a file in a torrent.',
  `album_id` int(11) NOT NULL COMMENT 'A foreign key to the p2pm_albums table.',
  `trackname` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Visible name of the track.',
  `plays` int(11) NOT NULL DEFAULT '0' COMMENT 'How often a song has been played. Tracks popularity + torrent availability',
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`),
  FULLTEXT KEY `trackname` (`trackname`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-18 20:15:35
