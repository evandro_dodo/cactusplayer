<?php
require_once "database.php";


if(isset($_GET['list_servers'])){

	echo listServers();

}elseif(isset($_GET['qtrack']) && isset($_GET['startpoint'])){
	if(!isset($_GET['server_request'])){
		askAnotherServerForContent($_GET['qtrack'], $_GET['startpoint']);
	}
	echo searchTrack($_GET['qtrack'], $_GET['startpoint']);

}elseif(isset($_GET['qalbum']) && isset($_GET['startpoint'])){
	if(!isset($_GET['server_request'])){
		askAnotherServerForContent($_GET['qalbum'], $_GET['startpoint']);
	}
	echo searchAlbum($_GET['qalbum'], $_GET['startpoint']);
}elseif(isset($_GET['album_urn'])){
	echo getAlbumTracksFromUrn($_GET['album_urn']);

}elseif(isset($_GET['rewardTrackLoaded'])&&isset($_GET['file_index'])){

	echo rewardTrackLoaded($_GET['rewardTrackLoaded'], $_GET['file_index']);
}elseif(isset($_GET['mostFavourite']) && isset($_GET['startpoint'])){

	echo loadMostFavouriteSongs($_GET['startpoint']);

}elseif(isset($_GET['getNewest']) && isset($_GET['startpoint'])){

	echo loadNewestSongs($_GET['startpoint']);

}elseif(isset($_GET['save_album_from_client'])){

	require_once "parser/parser_functions.php";

	echo saveAlbumFromClient();
}

if (isset($_GET['debug']) && mysql_errno()) {
	 $error = "MySQL error ".mysql_errno().": ".mysql_error()."\n<br>When executing:<br>\n$query\n<br>"; 
	 //echo $error;
}


?>